def ftoc(num)
  num = num.to_f
  result = (num-32.0) * (5.0/9.0)
  result.to_i
end

def ctof(num)
  num = num.to_f
  result = num * 1.8 + 32.0
  result
end
