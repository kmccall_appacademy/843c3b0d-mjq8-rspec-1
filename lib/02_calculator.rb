def add(num1,num2)
  num1+num2
end

def subtract(num1,num2)
  num1-num2
end

def sum(array)
  result = 0
  array.each do |num|
    result +=num
  end
  result
end

def multiply(array)
  result = 1
  array.each do |num|
    result *= num
  end
  result
end

def power(num1,num2)
  num1 ** num2
end

def factorial(number)
  result = 1
  (1..number).map do |num|
    result *= num
  end
  result
end
