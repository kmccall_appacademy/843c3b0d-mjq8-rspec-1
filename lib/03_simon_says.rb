def echo(string)
  string
end

def shout(string)
  string.upcase
end

def repeat(string, num = 2)
  result = (string + " ") * num
  result[0...-1]
end

def start_of_word(string, num = 1)
  string[0...num]
end

def first_word(string)
  result = string.split(" ")
  result[0]
end

def titleize(string)
  result = []
  string = string.capitalize
  string = string.split(" ")

  string.each do |word|
    if word == "and" or word == "over" or word == "the"
      result << word
    else
      result << word.capitalize
    end
  end
  result.join(" ")
end
