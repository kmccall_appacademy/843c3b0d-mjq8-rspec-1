def translate(string)
  string = string.split
  vowels = ("AaEeIiOoUu")
  result = []

  string.each do |word|
    if vowels.include?(word[0])
      result << word + "ay"
    elsif vowels.include?(word[1]) && word[0..1] != "qu"
      result << word[1..-1] + word[0] + "ay"
    elsif vowels.include?(word[2]) && word[1..2] != "qu"
      result << word[2..-1] + word[0..1] + "ay"
    else
      result << word[3..-1] + word[0..2] + "ay"
    end
  end
  result.join(" ")
end
